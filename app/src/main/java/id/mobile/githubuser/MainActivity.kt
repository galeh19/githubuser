package id.mobile.githubuser

import android.app.ProgressDialog
import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import androidx.core.widget.NestedScrollView
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import id.mobile.githubuser.modules.HttpRequest
import id.mobile.githubuser.modules.User
import id.mobile.githubuser.modules.UserItem
import id.mobile.githubuser.recycler.UserRecycler
import kotlinx.android.synthetic.main.activity_main.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MainActivity : AppCompatActivity() {
    lateinit var userList: ArrayList<User>
    lateinit var userRecycler: UserRecycler
    lateinit var progressDialog: ProgressDialog
    var PAGE: Int = 1
    var LIMIT = 0
    var requested = true
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        userList = ArrayList()
        userRecycler = UserRecycler(this, userList)

        val layoutManager = LinearLayoutManager(this)
        recycler_user.adapter = userRecycler
        recycler_user.layoutManager = layoutManager
        recycler_user.addItemDecoration(DividerItemDecoration(this, layoutManager.orientation))

        scroll_view.setOnScrollChangeListener(object : NestedScrollView.OnScrollChangeListener {
            override fun onScrollChange(v: NestedScrollView?, scrollX: Int, scrollY: Int, oldScrollX: Int, oldScrollY: Int) {
                var view = scroll_view.getChildAt(scroll_view.childCount - 1)
                var bottomDetector = view.getBottom() - (scroll_view.getHeight() + scroll_view.getScrollY())
                if(requested) {
                    if (bottomDetector <= 0) {
                        getData(PAGE + 1)
                    }
                }
            }
        })
        ic_search.setOnClickListener {
            getData(1)
        }
        et_search.setOnEditorActionListener {
                v, actionId, event ->
            if(actionId == EditorInfo.IME_ACTION_SEARCH){
                getData(1)
                true
            }
            false
        }

    }

    fun getData(page: Int){
        PAGE = page
        if(page == 1){
            LIMIT = 0
            requested = true
            userList.clear()
            userRecycler.notifyDataSetChanged()
        }
        val input: InputMethodManager = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        input.hideSoftInputFromWindow(et_search.windowToken, 0)
        progressDialogShow("Waiting", "Loading data...")
        HttpRequest().services.getUser(et_search.text.toString(), page.toString()).enqueue(object: Callback<UserItem> {
            override fun onFailure(call: Call<UserItem>, t: Throwable) {
                progressDialogDismiss()
                userList.clear()
                userRecycler.notifyDataSetChanged()
            }

            override fun onResponse(call: Call<UserItem>, response: Response<UserItem>) {
                progressDialogDismiss()
                if(response.code() == 200) {
                    val list = response.body()?.userList!!
                    if(LIMIT < list.size) LIMIT = list.size
                    else {
                        requested = false
                    }
                    userList.addAll(list)
                    userRecycler.notifyDataSetChanged()
                }
            }
        })
    }

    fun progressDialogShow(title: String, message: String) {
        progressDialog = ProgressDialog.show(this, title, message)
    }

    fun progressDialogDismiss() {
        if (progressDialog != null && progressDialog.isShowing())
            progressDialog.dismiss()
    }
}
