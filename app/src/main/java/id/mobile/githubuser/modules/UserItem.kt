package id.mobile.githubuser.modules

import com.google.gson.annotations.SerializedName

data class UserItem(
    @SerializedName("items")
    val userList: List<User>
)

data class User(
    @SerializedName("avatar_url")
    val image: String? = null,

    @SerializedName("login")
    val name: String? = null
)