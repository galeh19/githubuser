package id.mobile.githubuser.recycler

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import id.mobile.githubuser.R
import id.mobile.githubuser.modules.User
import kotlinx.android.synthetic.main.list_user.view.*

class UserRecycler(val context: Context, val itemList: List<User>): RecyclerView.Adapter<UserRecycler.ViewHolder>(){
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = itemList.get(position)
        holder.tvUser.text = item.name
        val options = RequestOptions()
//        options.placeholder(R.drawable.img_default)
//        options.error(R.drawable.img_default)
        Glide.with(context).load(item.image).apply(options).into(holder.imageUser)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(this.context).inflate(R.layout.list_user, parent, false))
    }

    override fun getItemCount(): Int {
        return itemList.size
    }
    class ViewHolder(view: View): RecyclerView.ViewHolder(view) {
        val imageUser = view.image_user
        val tvUser = view.tv_user
    }

}